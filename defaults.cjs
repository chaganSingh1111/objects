
const defaultFn = (testObject,defaultObj) => {
    if (testObject == null || testObject == undefined){
        return {}
      }
    if (typeof testObject != "object") {
        return {}
    }
    if(Array.isArray(testObject)){
        return {}
    }
    if (defaultObj == null || testObject == undefined){
        return {}
      }
    if (typeof defaultObj != "object") {
        return {}
    }
    if(Array.isArray(defaultObj)){
        return {}
    }

    for(let key in defaultObj ){
        if(testObject[key] == undefined){
           testObject[key] = defaultObj[key]
        } 
    }
    
    return testObject
}

module.exports = defaultFn