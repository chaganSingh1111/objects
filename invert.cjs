

const invert = (testObject) =>{
    if (testObject == null || testObject == undefined){
        return {}
      }
    if (typeof testObject != "object") {
        return {}
    }

    let invertObj = {}
    for(let key in testObject ){
        invertObj[testObject[key]] = key
    }
    
    return invertObj

}

module.exports = invert