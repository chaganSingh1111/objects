const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };


const pairs = (testObject) =>{
    let pairArray =[]
    let keysAsString = ' '
    if (testObject == null || testObject == undefined){
        return pairArray
      }
    if (typeof testObject != "object") {
        return pairArray
    }
    for(let key in testObject ){
        keysAsString=key
        pairArray.push([keysAsString,testObject[key]])    
    }
    
    return pairArray
}

module.exports = pairs