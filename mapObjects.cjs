const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };


const mapObject = (testObject,cb) =>{
    if (testObject == null || testObject == undefined){
        return {}
      }
    if (typeof testObject != "object") {
        return {}
    }

    for(let key in testObject ){
        if(typeof testObject[key] == "string"){
           testObject[key] = cb(testObject[key])
        } 
    }
    
    return testObject
}

module.exports = mapObject